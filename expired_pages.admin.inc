<?php

/**
 * @file expired_pages.admin.inc
 * 
 * Parent config page of expired_pages module.
 */

/**
 * Show config form on the Expired Pages module parent menu item.
 * @return The config form.
 */
function expired_pages_config_page() {
  $form['adminpage'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#description' => t('You must provide <a href="@ep-settings">configuration settings</a> before this module will run. By default this module runs as a Cron task at the interval specified in the settings. Alternatively, use the control below to switch it off and run it <a href="@ep-ondemand">on demand</a> instead.',array('@ep-settings' => url('admin/config/content/expired_pages/settings'),'@ep-ondemand' => url('admin/config/content/expired_pages/on_demand'))),
      '#title' => t('Expired Pages')
  );
  
  $form['adminpage']['expired_pages_cron'] = array(
  	'#type' => 'radios',
  	'#title' => t('Enable Expired Pages to run as a Cron task?'),
  	'#default_value' => variable_get('expired_pages_cron',1),
  	'#options' => array(0 => t('Disabled'), 1=>t('Enabled'))	
  );
  
  return system_settings_form($form);
}

/**
 * Assemble Expired Pages module config form.
 * @return Array A form for rendering by the Form API.
 */
function _expired_pages_config_form(){
  $defaultMailText = "Dear !author,

This is to remind you that your webpage entitled '!pageTitle' has not changed since !changed.

Could you please check that it is still up to date? To stop receiving these messages, log on and save the page after you have reviewed its contents.

Many thanks!

The website.";

  // housekeeping to ensure content types are up-to-date
  // get the current list and a fresh (definitive) list
  $contentTypes = variable_get('expired_pages_types',FALSE);
  $latestContentTypes = db_query('SELECT type, name FROM node_type ORDER BY name');
  $freshContentTypes = array();
  foreach ($latestContentTypes as $contentType){
    $freshContentTypes[$contentType->type] = $contentType->name;
  }
  // if no current list, then make the current list the fresh list
  if(!$contentTypes){
    $contentTypes = $freshContentTypes;
  } else {
    // else remove from all lists any in the current list that are not in the fresh list
    foreach($contentTypes as $key => $value) {
      $foundInFreshList = array_search($value,$freshContentTypes);
      if($foundInFreshList===FALSE){
        unset($contentTypes[$key]);
        variable_del('expired_pages_type_'.$key);
      } else {
        // remove any that are in both from the fresh list
        unset($freshContentTypes[$foundInFreshList]);
      }
    }
    // merge current list with what's left of the fresh list
    $contentTypes = array_merge($contentTypes, $freshContentTypes);
  }
  $form['timings'] = array(
      '#type' => 'fieldset',
      '#collapsible' => FALSE,
      '#description' => t('Set expiry age and reminder interval.'),
      '#title' => t('Timings')
  );
  $form['timings']['expired_pages_age'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('expired_pages_age', ''),
    '#title' => t('Age'),
    '#description' => t('Relative age at which nodes will be regarded as expired. Specify units below.'),
    '#element_validate' => array('element_validate_integer_positive')
  );
  $form['timings']['expired_pages_age_unit'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#default_value' => variable_get('expired_pages_age_unit', ''),
    '#title' => t('Age unit'),
    '#options' => drupal_map_assoc(array('Days','Weeks','Months'))
  );
  $form['timings']['expired_pages_interval'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#default_value' => variable_get('expired_pages_interval', ''),
    '#title' => t('Interval'),
    '#description' => t('Frequency at which the site will be checked for expired content. Specify units below.'),
    '#element_validate' => array('element_validate_integer_positive')
  );
  $form['timings']['expired_pages_interval_unit'] = array(
    '#type' => 'select',
    '#required' => TRUE,
    '#default_value' => variable_get('expired_pages_interval', ''),
    '#title' => t('Interval unit'),
    '#options' => drupal_map_assoc(array('Days','Weeks','Months'))
  );
  $form['types'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#description' => t('Specify content types to be checked.'),
    '#title' => t('Content Types')
  );
  foreach($contentTypes as $key => $value) {
    $form['types']['expired_pages_type_'.$key] = array(
      '#type' => 'checkbox',
      '#title' => t($value),
      '#default_value' => variable_get('expired_pages_type_'.$key, FALSE)
     );
  }
  $form['types']['expired_pages_types'] = array(
    '#type' => 'hidden',
    '#value' => $contentTypes
  );
  $form['mailBody'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#title' => t('Expiry Email')
  );
  $form['mailBody']['expired_pages_mailsubject'] = array(
    '#type' => 'textfield',
    '#title' => t('The subject line of the reminder email.'),
    '#default_value' => variable_get('expired_pages_mailsubject', 'Web page review reminder'),
    '#description' => t('Placeholders are recognised. See below for details.')
  );
  $form['mailBody']['expired_pages_mailtext'] = array(
    '#type' => 'textarea',
    '#title' => t('The text of the reminder email.'),
    '#default_value' => variable_get('expired_pages_mailtext', $defaultMailText),
    '#description' => t('The following placeholders are recognised: !author (the user account name), !url (the node path/alias), !pageTitle (the node title) and !changed (the last changed date).')
  );
  return $form;
}
