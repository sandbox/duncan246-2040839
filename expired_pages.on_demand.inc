<?php

/**
 * Construct form expired_pages_on_demand.
 * @return Array containing the form.
 */
function expired_pages_on_demand() {

  $config = _expired_pages_get_config();
  if($config) {
  	$currentInterval = '('.$config['interval'].' '.$config['interval_unit'].')';
  } else {
  	$currentInterval = '';
  }
  
  $form['ondemand'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#description' => t('Send reminder emails right away, based on <a href="@ep-settings">current settings</a>. This will not affect the timing of the next scheduled run. Emails will not be sent to any user who has already received one within the currently specified interval. @interval-value', array('@ep-settings' => url('admin/config/content/expired_pages/settings'), '@interval-value' => $currentInterval)),
    '#title' => t('On Demand')
  );
  $form['ondemandbuttons'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE
  );
  $form['ondemandbuttons']['no'] = array(
    '#type' => 'button',
    '#value' => t("No thanks, I've changed my mind."),
    '#button_type' => 'submit',
    '#executes_submit_callback' => TRUE,
    '#submit' => array('expired_pages_on_demand_cancel'),
  );
  $form['ondemandbuttons']['go'] = array(
    '#type' => 'button',
    '#value' => t("Yes, let's do it!"),
    '#button_type' => 'submit',
    '#executes_submit_callback' => TRUE,
    '#submit' => array('expired_pages_on_demand_go'),
  );
  return $form;
}


/**
 * Callback for cancel button on form expired_pages_on_demand.
 */
function expired_pages_on_demand_cancel(){
  drupal_goto('admin/config/content/expired_pages');
}


/**
 * Callback for go button on form expired_pages_on_demand.
 */
function expired_pages_on_demand_go(){
  _expired_pages_do('on_demand');
}

